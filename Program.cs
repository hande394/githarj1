﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ohjelma1
{
    class Program
    {
        static void Main(string[] args)
        {
            double summa = 0;
            for (int i = 0; i < 3; i++)
            {
                Console.Write("Name?");
                string name = Console.ReadLine();
                Console.Write("Age?");
                int age = Convert.ToInt32(Console.ReadLine());
                summa += age;
                Console.WriteLine("{0} is {1} years old", name, age);


            }
            Console.WriteLine("Keski-ikä on " + Math.Round(summa / 3));
            
            Console.ReadLine();
        }
    }
}
